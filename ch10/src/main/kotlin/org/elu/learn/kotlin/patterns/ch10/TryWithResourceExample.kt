package org.elu.learn.kotlin.patterns.ch10

import java.io.BufferedReader
import java.io.FileReader

fun main() {
    val br = BufferedReader(FileReader("gradle.properties"))

    br.use {
        println(it.readLine())
    }
    println("Done!")
}
