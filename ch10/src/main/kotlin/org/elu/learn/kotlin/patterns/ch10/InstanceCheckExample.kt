package org.elu.learn.kotlin.patterns.ch10

fun main() {
    val b = Batman()
    val s = Superman()

    doCoolStuffJavaWay(b)
    doCoolStuffJavaWay(s)

    doCoolStuffSmartCast(b)
    doCoolStuffSmartCast(s)

    doCoolStuff(b)
    doCoolStuff(s)

    val superheroAsString = (s as? String) // Superhero not a string, this will be null
    println(superheroAsString)
}

interface Superhero
class Batman : Superhero {
    fun callRobin() {
        println("To the Bat-pole, Robin!")
    }
}

class Superman : Superhero {
    fun fly() {
        println("Up, up and away!")
    }
}

fun doCoolStuffJavaWay(s: Superhero) {
    if (s is Superman) {
        (s as Superman).fly()
    } else if (s is Batman) {
        (s as Batman).callRobin()
    }
}

fun doCoolStuffSmartCast(s: Superhero) {
    if (s is Superman) {
        s.fly()
    } else if (s is Batman) {
        s.callRobin()
    }
}

fun doCoolStuff(s: Superhero) {
    when(s) {
        is Superman -> s.fly()
        is Batman -> s.callRobin()
        else -> println("Unknown superhero")
    }
}
