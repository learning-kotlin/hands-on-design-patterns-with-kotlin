package org.elu.learn.kotlin.patterns.ch10

fun main() {
    val myclass1 = MyClass()
    println("${myclass1.a} ${myclass1.b} ${myclass1.c}")

    val myclass2 = MyClass("Two")
    println("${myclass2.a} ${myclass2.b} ${myclass2.c}")

    val myclass3 = MyClass("Three", 2)
    println("${myclass3.a} ${myclass3.b} ${myclass3.c}")

    val myclass4 = MyClass("Four", 3, 1L)
    println("${myclass4.a} ${myclass4.b} ${myclass4.c}")

    val better = BetterClass()
    println("${better.a} ${better.b} ${better.c}")

    val better1 = BetterClass(b = 5)
    println("${better1.a} ${better1.b} ${better1.c}")

    val better2 = BetterClass("Better Two", 5, 6L)
    println("${better2.a} ${better2.b} ${better2.c}")

    val better3 = BetterClass(a = "Better Three", c = 5L, b = 6)
    println("${better3.a} ${better3.b} ${better3.c}")
}

// Java style
class MyClass(val a: String, val b: Int, val c: Long) {

    constructor(a: String, b: Int) : this(a, b, 0)

    constructor(a: String) : this(a, 1)

    constructor() : this("Default")
}

class BetterClass(val a: String = "Better Default",
                  val b: Int = 1,
                  val c: Long = 0)

