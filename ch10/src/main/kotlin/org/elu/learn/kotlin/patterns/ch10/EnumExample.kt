package org.elu.learn.kotlin.patterns.ch10

import org.elu.learn.kotlin.patterns.ch10.PizzaOrderStatus.*

fun main() {
    var status: PizzaOrderStatus = OrderReceived(123)

    while (status !is Completed) {
        status = when (status) {
            is OrderReceived -> status.nextStatus()
            is PizzaBeingMade -> status.nextStatus()
            is OutForDelivery -> status.nextStatus()
            is Completed -> status
        }
    }
}

// Java code, overload enum with functionality
internal enum class PizzaOrderStatusJava {
    ORDER_RECEIVED,
    PIZZA_BEING_MADE,
    OUT_FOR_DELIVERY,
    COMPLETED;


    fun nextStatus(): PizzaOrderStatusJava {
        return when (this) {
            ORDER_RECEIVED -> PIZZA_BEING_MADE
            PIZZA_BEING_MADE -> OUT_FOR_DELIVERY
            OUT_FOR_DELIVERY -> COMPLETED
            COMPLETED -> COMPLETED
        }
    }
}

// Better to use the sealed class
sealed class PizzaOrderStatus(protected val orderId: Int) {
    abstract fun nextStatus(): PizzaOrderStatus
    class OrderReceived(orderId: Int) : PizzaOrderStatus(orderId) {
        override fun nextStatus(): PizzaOrderStatus {
            return PizzaBeingMade(orderId)
        }
    }

    class PizzaBeingMade(orderId: Int) : PizzaOrderStatus(orderId) {
        override fun nextStatus(): PizzaOrderStatus {
            return OutForDelivery(orderId)
        }
    }

    class OutForDelivery(orderId: Int) : PizzaOrderStatus(orderId) {
        override fun nextStatus(): PizzaOrderStatus {
            return Completed(orderId)
        }
    }

    class Completed(orderId: Int) : PizzaOrderStatus(orderId) {
        override fun nextStatus(): PizzaOrderStatus {
            return this
        }
    }
}
