package org.elu.learn.kotlin.patterns.ch10

fun main() {
    makesSense {
        "Inlining"
    }
}

inline fun doesntMakeSense(something: String) {
    println(something)
}

inline fun makesSense(block: () -> String) {
    println("===Before===")
    println(block())
    println("===After===")
}
