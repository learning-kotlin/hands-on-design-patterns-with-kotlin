package org.elu.learn.kotlin.patterns.ch10

import kotlin.random.Random

fun main() {
    val sometimesNull = if (Random.nextBoolean()) "not null" else null
    sometimesNull?.let {
        println("It was $it this time")
    }

    val alwaysNull = null
    alwaysNull.let {// No null pointer there
        println("It was $it this time") // Always prints null
    }

    val justAString = "string"

    val numberReturned = justAString.let {
        println(it)
        it.length
    }

    println(numberReturned)
}
