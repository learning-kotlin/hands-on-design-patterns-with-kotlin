package org.elu.learn.kotlin.patterns.ch10

fun main() {
    // java way
    val agentJavaWay = JamesBond()
    agentJavaWay.name = "Sean Connery"
    agentJavaWay.movie = "Dr. No"

    // using apply
    val `007` = JamesBond().apply {
        this.name = "Sean Connery"
        this.movie = "Dr. No"
    }
    println(`007`.name)

    // or even shorter
    val agent = JamesBond().apply {
        name = "Sean Connery"
        movie = "Dr. No"
    }
    println(agent.movie)
}

class JamesBond {
    lateinit var name: String
    lateinit var movie: String
    lateinit var alsoStarring: String
}
