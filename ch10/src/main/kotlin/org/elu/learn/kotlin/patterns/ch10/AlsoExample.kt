package org.elu.learn.kotlin.patterns.ch10

fun main() {
    println(multiply(2, 3))
    multiplyAndPrint(2, 3)
    multiplyAlso(2, 3)

    val l = (1..100).toList()
    l.filter { it % 2 == 0 }
        .also { println(it) } // Prints, but doesn't change anything
        .map { it * it }
}

fun multiply(a: Int, b: Int): Int = a * b

fun multiplyAndPrint(a: Int, b: Int): Int {
    val c = a * b
    println(c)
    return c
}

fun multiplyAlso(a: Int, b: Int): Int = (a * b).also { println(it) }
