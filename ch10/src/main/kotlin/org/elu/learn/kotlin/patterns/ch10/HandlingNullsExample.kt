package org.elu.learn.kotlin.patterns.ch10

import kotlin.random.Random

fun main() {
    // Will return "String" half of the time, and null the other half
    val stringOrNull: String? = if (Random.nextBoolean()) "String" else null

    // Java-way check
    if (stringOrNull != null) {
        println(stringOrNull.length)
    }

    // using elvis operator ?:
    val alwaysLength = stringOrNull?.length ?: 0

    println(alwaysLength) // Will print 6 or 0, but never null

    val json: Json? = Json(Profile(null, null))

    println(json?.User?.firstName?.length)

    // using let
    println(json?.let {
        it.User?.let {
            it.firstName?.length
        }
    })

    // using run, no need for explicit it call
    println(json?.run {
        User?.run {
            firstName?.length
        }
    })
}

data class Json(val User: Profile?)

data class Profile(val firstName: String? = null,
                   val lastName: String? = null)
