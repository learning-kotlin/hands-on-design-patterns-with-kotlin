package org.elu.learn.kotlin.patterns.ch10

fun main() {
    println("Say ${hello()}") // this doesn't print "hello"
    println("Say ${hello()()}") // this does print "hello"
}

// Scala style function
fun hello() = {
    "hello"
}

fun helloExpanded(): () -> String {
    return {
        "hello"
    }
}

fun helloExpandedMore(): () -> String {
    return fun(): String {
        return "hello"
    }
}
