package org.elu.learn.kotlin.patterns.ch10

fun main() {
    println(Spock.SENSE_OF_HUMOR)

    println(SensesOfHumor.SPOCK)
}

class Spock {
    companion object {
        const val SENSE_OF_HUMOR = "NONE"
    }
}

object SensesOfHumor {
    const val SPOCK = "NONE"
}
