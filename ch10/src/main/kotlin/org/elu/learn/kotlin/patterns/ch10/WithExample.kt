package org.elu.learn.kotlin.patterns.ch10

fun main() {
    // "scope".with {} // this doesn't compile
    with("scope") {
        println(this.length)
    }
    // this can be omitted
    val result = with("string") {
        length
    }
    println(result)
}
