package org.elu.learn.kotlin.patterns.ch10

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    println("Name: ${getName()}")

    println("Name: ${getNameAsAsync().await()}")
}

fun getName() = GlobalScope.async {
    delay(100)
    "Edu"
}

fun getNameAsAsync() = GlobalScope.async {
    delay(100)
    "Edu"
}
