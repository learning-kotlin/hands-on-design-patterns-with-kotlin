package org.elu.learn.kotlin.patterns.ch10

fun main() {
    val justAString = "string"
    val n = justAString.run {
        this.length
    }
    println(n)

    val m = justAString.run {
        length
    }
    println(m)

    val year = JamesBond().run {
        name = "ROGER MOORE"
        movie = "THE MAN WITH THE GOLDEN GUN"
        1974 // Not JamesBond type
    }
    println(year)
}
