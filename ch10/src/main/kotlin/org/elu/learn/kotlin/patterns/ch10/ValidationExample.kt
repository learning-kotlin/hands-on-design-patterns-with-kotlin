package org.elu.learn.kotlin.patterns.ch10

fun main() {
    try {
        setCapacityJava(-1)
    } catch (e: Exception) {
        println("Java Exception caught: $e")
    }
    try {
        setCapacity(-1)
    } catch (e: Exception) {
        println("Exception caught: $e")
    }
    setCapacity(1)

    val p = Profile(firstName = "Edu")
    printNameLength(p)
    try {
        printNameLength(Profile())
    } catch (e: Exception) {
        println("Name Exception caught: $e")
    }

    val client = HttpClient()

    try {
        client.postRequest()
    } catch (e: Exception) {
        println("Failed to post request: $e")
    }

    client.body = ""
    client.postRequest()
}

fun setCapacityJava(cap: Int) {
    if (cap < 0) {
        throw IllegalArgumentException()
    }
}

fun setCapacity(cap: Int) {
    require(cap > 0)
}

fun printNameLength(p: Profile) {
    //require(p.firstName != null)
    requireNotNull(p.firstName)
}

private class HttpClient {
    var body: String? = null
    var url: String = ""

    fun postRequest() {
        check(body != null) {
            "Body must be set in POST requests"
        }
        println("Post request sent")
    }

    fun getRequest() {
        // This one is fine without body
    }
}
