package org.elu.learn.kotlin.patterns.ch10

import kotlin.reflect.KClass

fun main() {
    printIfSameTypeJava(Int::class, 1) // Print 1, as 1 is Int
    printIfSameTypeJava(Int::class, 2L) // Prints nothing, as 2 is Long

    printIfSameTypeReified<Int>(3) // Prints 3, as 3 is Int
    printIfSameTypeReified<Int>(4L) // Prints nothing, as 4 is Long
    printIfSameTypeReified<Long>(5) // Prints nothing, as 5 is Int
    printIfSameTypeReified<Long>(6L) // Prints 6, as 6 is Long

    printList(listOf(1, 2, 3, 4))
    printList(listOf(1L, 2L, 3L, 4L))
    printList(listOf("a", "b", "c"))
}

/*
fun <T> printIfSameType(a: Number) {
    if (a is T) { // this will not compile, because generic types are erased
        println(a)
    }
}*/

// Java style implementation
fun <T: Number> printIfSameTypeJava(clazz: KClass<T>, a: Number) {
    if (clazz.isInstance(a)) {
        println(a)
    }
}

// using reified
inline fun <reified T> printIfSameTypeReified(a: Number) {
    if (a is T) {
        println(a)
    }
}

/* Next 2 functions won't compile because of same signature
   Again generics are erased during compilation
fun printList(list: List<Int>) {
    println("This is a lit of Ints")
    println(list)
}

fun printList(list: List<Long>) {
    println("This is a lit of Longs")
    println(list)
}*/

//fixing above problem with reified
inline fun <reified T: Any> printList(list: List<T>) {
    when(T::class) {
        Int::class -> println("This is a list of Ints")
        Long::class -> println("This is a list of Longs")
        else -> println("This is a list of something else")
    }
    println(list)
}