package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    val p1p2 = Channel<Int>()
    val p2p1 = Channel<Int>()

    player("Player 1", p2p1, p1p2)
    player("Player 2", p1p2, p2p1)

    runBlocking {
        p2p1.send(0)
        delay(1000L)
    }
}

fun player(name: String,
           input: Channel<Int>,
           output: Channel<Int>) = GlobalScope.launch {
    for (m in input) {
        val d = (1..100).shuffled().first()
        println("$name got $m, ${if (d > m) "won" else "lost" }")

        delay(d.toLong())
        output.send(d)
    }
}
