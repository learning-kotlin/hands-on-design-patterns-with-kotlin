package org.elu.learn.kotlin.patterns.ch08

import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread

fun main() {
    val counter = AtomicInteger()
    try {
        for (i in 0..10_000) {
            thread {
                counter.incrementAndGet()
                Thread.sleep(100)
            }
        }
    } catch (ex: OutOfMemoryError) {
        println("Spawned ${counter.get()} threads before crashing")
        System.exit(-42)
    }
}
