package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() {
    val userProfile = GlobalScope.async {
        delay((1..100L).shuffled().first())
        "Profile"
    }

    val userHistory = GlobalScope.async {
        delay((1..200L).shuffled().first())
        listOf(1, 2, 3)
    }

    runBlocking {
        println("User profile is ${userProfile.await()} and his history is ${userHistory.await()}")
    }
}
