package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.yield
import java.util.*

fun main() = runBlocking {
    val parentJob = Job()

    List(10) {
        GlobalScope.async(Dispatchers.Default + parentJob) {
            produceBeautifulUuid()
        }
    }

    delay(100L)
    parentJob.cancel()
    delay(1000L)
}

suspend fun produceBeautifulUuid(): String {
    try {
        val uuids = List(1000) {
            yield()
            UUID.randomUUID()
        }

        println("Coroutine done")
        return uuids.sorted().first().toString()
    } catch (t: CancellationException) {
        println("Got cancelled")
    }

    return ""
}