package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.yield

fun main() {
    val j = GlobalScope.launch {
        for (i in 1..10_000) {
            if (i % 1000 == 0) {
                println(i)
                yield()
            }
        }
    }

    runBlocking {
        j.join()
    }
}
