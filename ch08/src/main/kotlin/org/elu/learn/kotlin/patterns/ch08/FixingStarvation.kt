package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import java.util.*
import java.util.concurrent.CountDownLatch

fun main() {
    val latch = CountDownLatch(10 * 2)
    for (i in 1..10) {
        GlobalScope.launch {
            CoroutineFactory.longCoroutine(i)
            latch.countDown()
        }
    }

    List(10) { i ->
        GlobalScope.launch {
            CoroutineFactory.shortCoroutine(i)
            latch.countDown()
        }
    }

    latch.await()
}

suspend fun CoroutineFactory.longCoroutine(index: Int) {
    var uuid = UUID.randomUUID()
    for (i in 1..100_000) {
        val newUuid = UUID.randomUUID()

        if (newUuid < uuid) {
            uuid = newUuid
        }

        if (i % 100 == 0) {
            yield()
        }
    }

    println("Done longCoroutine $index")
}
