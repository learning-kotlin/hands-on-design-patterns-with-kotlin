package org.elu.learn.kotlin.patterns.ch08

import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread

fun main() {
    notSafe()
    safe()
}

fun notSafe() {
    println("Not safe example")
    var counter = 0
    val latch = CountDownLatch(100_000)

    for (i in 1..100_000) {
        thread {
            counter++
            latch.countDown()
        }
    }
    latch.await()
    println("Counter $counter")
}

fun safe() {
    println("Safe example")
    var counter = 0
    val latch = CountDownLatch(100_000)

    for (i in 1..100_000) {
        thread {
            synchronized(latch) {
                counter++
                latch.countDown()
            }
        }
    }
    latch.await()
    println("Counter $counter")
}
