package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout

fun main() = runBlocking {
    val coroutine = GlobalScope.async {
        withTimeout(500) {
            try {
                val time = (1..1000).shuffled().first().toLong()
                println("It will take me $time to do")

                delay(time)

                println("Returning profile")
                "Profile"
            } catch (e: TimeoutCancellationException) {
                e.printStackTrace()
            }
        }
    }

    val result = try {
        coroutine.await()
    } catch (e: TimeoutCancellationException) {
        "No Profile"
    }

    println(result)
}
