package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

@ExperimentalCoroutinesApi
fun main() {
    val publisher: ReceiveChannel<Int> = GlobalScope.produce {
        for (i in 2019 downTo 1970) { // Years back to Unix
            send(i)
            delay(20L)
        }
    }

    runBlocking {
        publisher.consumeEach {
            println("Got $it")
        }
    }
}
