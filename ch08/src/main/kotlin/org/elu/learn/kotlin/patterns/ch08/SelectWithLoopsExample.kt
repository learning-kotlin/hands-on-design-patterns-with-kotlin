package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select

@ExperimentalCoroutinesApi
fun main() {
    runBlocking {
        println("without loop")
        withoutLoop()
        delay(1000L)
        println("with loop")
        withLoop()
        delay(1000L)
        println("with loop and close")
        withLoopAndClose()
    }
}

@ExperimentalCoroutinesApi
suspend fun withoutLoop() {
    // Producer 1
    val firstProducer = GlobalScope.produce {
        for (c in 'a'..'z') {
            delay((1..100L).shuffled().first())
            send(c.toString())
        }
    }

// Producer 2
    val secondProducer = GlobalScope.produce {
        for (c in 'A'..'Z') {
            delay((1..100L).shuffled().first())
            send(c.toString())
        }
    }

// Receiver
    println(select<String> {
        firstProducer.onReceive {
            it
        }
        secondProducer.onReceive {
            it
        }
    })
}

@ExperimentalCoroutinesApi
suspend fun withLoop() {
    // Producer 1
    val firstProducer = GlobalScope.produce {
        for (c in 'a'..'z') {
            delay((1..100L).shuffled().first())
            send(c.toString())
        }
    }

// Producer 2
    val secondProducer = GlobalScope.produce {
        for (c in 'A'..'Z') {
            delay((1..100L).shuffled().first())
            send(c.toString())
        }
    }

// Receiver
    for (i in 1..10) { // reads only 1 letters
        println(select<String> {
            firstProducer.onReceive {
                it
            }
            secondProducer.onReceive {
                it
            }
        })
    }
}

@ExperimentalCoroutinesApi
suspend fun withLoopAndClose() {
    // Producer 1
    val firstProducer = GlobalScope.produce {
        for (c in 'a'..'z') {
            delay((1..100L).shuffled().first())
            send(c.toString())
        }
        close()
    }

// Producer 2
    val secondProducer = GlobalScope.produce {
        for (c in 'A'..'Z') {
            delay((1..100L).shuffled().first())
            send(c.toString())
        }
        close()
    }

// Receiver
    while (true) { // runs until any of producers is closed
        val result = select<String?> {
            firstProducer.onReceiveOrNull {
                it
            }
            secondProducer.onReceiveOrNull {
                it
            }
        }

        if (result == null) {
            break
        } else {
            println(result)
        }
    }
}
