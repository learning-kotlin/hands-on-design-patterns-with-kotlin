package org.elu.learn.kotlin.patterns.ch08

import kotlin.concurrent.thread

fun main() {
    runThreads()
}

fun runThreads() {
    val t1 = thread { // this thread will start automatically
        for (i in 1..100) {
            println("T1: $i")
        }
    }

    val t2 = thread(start = false) { // this thread must be started explicitly
        for (i in 1..100) {
            println("T2: $i")
        }
    }
    t2.start()

    val t3 = thread(isDaemon = true) { // this thread is running as daemon, will stops with parent thread
        for (i in 1..1_000) {
            println("T3: $i")
        }
    }
}
