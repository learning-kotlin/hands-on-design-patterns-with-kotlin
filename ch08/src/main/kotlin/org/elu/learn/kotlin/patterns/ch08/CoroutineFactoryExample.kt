package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.*
import java.util.concurrent.CountDownLatch

fun main() {
    val latch = CountDownLatch(10 * 2)
    for (i in 1..10) {
        GlobalScope.async {
            CoroutineFactory.greedyLongCoroutine(i)
            latch.countDown()
        }
    }

    for (i in 1..10) {
        GlobalScope.async {
            CoroutineFactory.shortCoroutine(i)
            latch.countDown()
        }
    }

    latch.await()
}

object CoroutineFactory {
    fun greedyLongCoroutine(index: Int) {
        var uuid = UUID.randomUUID()
        for (i in 1..100_000) {
            val newUuid = UUID.randomUUID()

            if (newUuid < uuid) {
                uuid = newUuid
            }
        }
        println("Done greedyLongCoroutine $index")
    }

    fun shortCoroutine(index: Int) {
        println("Done shortCoroutine $index!")
    }
}