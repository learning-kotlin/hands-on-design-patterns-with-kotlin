package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.*

fun main() {
    val cancellable = GlobalScope.launch {
        try {
            for (i in 1..1000) {
                println("Cancellable: $i")
                yield()
            }
        }
        catch (e: CancellationException) {
            e.printStackTrace()
        }
    }

    val notCancellable = GlobalScope.launch {
        for (i in 1..1000) {
            if (i % 100 == 0) {
                println("Not cancellable $i")
            }
        }
    }

    println("Canceling cancellable")
    cancellable.cancel()
    println("Canceling not cancellable")
    notCancellable.cancel()


    runBlocking {
        cancellable.join()
        notCancellable.join()
    }
}
