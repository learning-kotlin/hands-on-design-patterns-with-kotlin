package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select

@ExperimentalCoroutinesApi
fun main() = runBlocking {
    val firstProducer = GlobalScope.produce {
        delay((1..100L).shuffled().first())
        send("First")
        println("firstProducer done")
    }

    val secondProducer = GlobalScope.produce {
        delay((1..100L).shuffled().first())
        send("Second")
        println("secondProducer done")
    }

    val winner = select<String> {
        firstProducer.onReceive {
            it.toLowerCase()
        }
        secondProducer.onReceive {
            it.toUpperCase()
        }
    }

    println(winner)
}
