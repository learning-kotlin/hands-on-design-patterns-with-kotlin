package org.elu.learn.kotlin.patterns.ch08

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.runBlocking

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
fun main() {
    val me = GlobalScope.actor<Task> {
        while (!isClosedForReceive) {
            println(receive().description.repeat(10))
        }
    }
    println("First me")
    michael(me)

    // preferable, if the actor receives tasks from many managers
    val meAgain = GlobalScope.actor<Task> {
        var next = receiveOrNull()

        while (next != null) {
            println(next.description.toUpperCase())
            next = receiveOrNull()
        }
    }
    println("Me again")
    michael(meAgain)

    // the most preferable
    val meWithRange = GlobalScope.actor<Task> {
        for (t in channel) {
            println(t.description)
        }
        println("Done everything")
    }
    println("Me with range")
    michael(meWithRange)
}

data class Task(val description: String)
fun michael(actor: SendChannel<Task>) {
    runBlocking {
        for (i in 'a'..'z') {
            actor.send(Task(i.toString()))
        }
        actor.close()
    }
}
