package org.elu.learn.kotlin.patterns.catsShelter

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.junit5.VertxExtension
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServerVerticleTest {
    private val vertx: Vertx = Vertx.vertx()

    @BeforeAll
    fun setup() {
        startServer()
    }

    private fun startServer() {
        val d1 = CompletableDeferred<String>()
        vertx.deployVerticle(ServerVerticle()) {
            d1.complete("OK")
        }
        runBlocking {
            println("Server started")
            d1.await()
        }
    }

    @AfterAll
    fun tearDown() {
        vertx.close()
    }

    @Test
    fun `Tests that alive works`() {
        val response = get("/alive")

        assertNotNull(response)
        assertEquals(response.statusCode(), 200)

        val body = response.asJson()
        assertNotNull(body["alive"])
        assertEquals(body["alive"].booleanValue(), true)
    }

    @Test
    fun `Makes sure cat can be created`() {
        val response = post("/api/v1/cats",
            """
                {
                    "name": "Binky",
                    "age": 5
                }
                """)

        assertNotNull(response)
        assertEquals(201, response.statusCode())
        val body = response.asJson()
        assertNotNull(body["id"])
        assertNotNull(body["name"])
        assertEquals("Binky", body["name"].textValue())
        assertNotNull(body["age"])
        assertEquals(5, body["age"].intValue())
    }

    @Test
    fun `Make sure that all cats are returned`() {
        val response = get("/api/v1/cats")

        assertNotNull(response)
        assertEquals(response.statusCode(), 200)
        val body = response.asJson()
        assertTrue(body.size() > 0)
    }

    private fun get(path: String): HttpResponse<Buffer> {
        val d1 = CompletableDeferred<HttpResponse<Buffer>>()

        val client = WebClient.create(vertx)
        client.get(8080, "localhost", path).send {
            d1.complete(it.result())
        }

        return runBlocking {
            d1.await()
        }
    }

    private fun post(path: String, body: String = ""): HttpResponse<Buffer> {
        val d1 = CompletableDeferred<HttpResponse<Buffer>>()

        val client = WebClient.create(vertx)
        client.post(8080, "localhost", path).sendBuffer(Buffer.buffer(body)) { res ->
            d1.complete(res.result())
        }

        return runBlocking {
            d1.await()
        }
    }
}

private fun <T> HttpResponse<T>.asJson(): JsonNode {
    return this.bodyAsBuffer().asJson()
}

private fun Buffer.asJson(): JsonNode {
    return ObjectMapper().readTree(this.toString())
}
