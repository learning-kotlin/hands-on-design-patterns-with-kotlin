package org.elu.learn.kotlin.patterns.catsShelter

import io.vertx.core.AsyncResult
import io.vertx.core.MultiMap
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.jdbc.JDBCClient
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.CoroutineVerticle

private val insert = """insert into cats (name, age)
            |values (?, ?::integer) RETURNING *""".trimMargin()

class ServerVerticle: CoroutineVerticle() {
    override suspend fun start() {
        val router = router()

        // Start the server
        vertx.createHttpServer()
            .requestHandler(router)
            .listenAwait(config.getInteger("http.port", 8080))
    }

    private fun router(): Router {
        val router = Router.router(vertx)
        val dbClient = getDbClient()

        router.route("/*").handler(BodyHandler.create())
        router.get("/").coroutineHandler { ctx -> ctx.response().end("Welcome!") }
        router.get("/alive").coroutineHandler { ctx -> handleAlive(ctx, dbClient) }
        router.mountSubRouter("/api/v1", apiRouter(dbClient))

        return router
    }

    private fun apiRouter(dbClient: JDBCClient): Router {
        val router = Router.router(vertx)
        router.post("/cats").coroutineHandler { ctx -> createCat(ctx, dbClient) }
        router.get("/cats").coroutineHandler { ctx -> getCats(ctx) }
        router.get("/cats/:id").coroutineHandler {ctx -> getCats(ctx) }
        return router
    }

    private suspend fun handleAlive(ctx: RoutingContext, dbClient: JDBCClient) {
        val dbAlive = dbClient.query("select true as alive")
        val json = json {
            obj(
                "alive" to true,
                "db" to dbAlive.await()["rows"]
            )
        }
        ctx.response().endWithJson(json)
    }

    private fun createCat(ctx: RoutingContext, dbClient: JDBCClient) {
        dbClient.queryWithParams(insert, ctx.bodyAsJson.toCat()) {
            it.handle({
                // We'll always have one result here, since it's our row
                ctx.respond(it.result().rows[0].toString(), 201)
            }, {
                println("ERROR: $it")
                ctx.respond(status = 500)
            })
        }
    }

    private fun getCats(ctx: RoutingContext) {
        send(CATS, ctx.request().params().toJson()) {
            it.handle({
                val responseBody = it.result().body()
                ctx.respond(responseBody.get<JsonArray>("rows").toString())
            }, {
                ctx.respond(status=500)
            })
        }
    }
}

private fun JsonObject.toCat() = JsonArray().apply {
    add(this@toCat.getString("name"))
    add(this@toCat.getInteger("age"))
}

fun <T> CoroutineVerticle.send(address: String,
                               message: T,
                               callback: (AsyncResult<Message<T>>) -> Unit) {
    this.vertx.eventBus().send(address, message, callback)
}

private fun MultiMap.toJson(): JsonObject {
    val json = JsonObject()

    for (k in this.names()) {
        json.put(k, this[k])
    }

    return json
}
