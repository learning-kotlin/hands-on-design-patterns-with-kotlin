package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.runBlocking

@ExperimentalCoroutinesApi
fun main() {
    val producer = producePagesFanOut()

    val consumers = List(10) {
        consumePages(producer)
    }

    runBlocking {
        consumers.forEach {
            it.await()
        }
    }
}

@ExperimentalCoroutinesApi
private fun producePagesFanOut() = GlobalScope.produce {
    for (i in 1..10_000) {
        for (c in 'a'..'z') {
            send(i to "page$c")
        }
    }
}

private fun consumePages(channel: ReceiveChannel<Pair<Int, String>>) = GlobalScope.async {
    for (p in channel) {
        println(p)
    }
}
