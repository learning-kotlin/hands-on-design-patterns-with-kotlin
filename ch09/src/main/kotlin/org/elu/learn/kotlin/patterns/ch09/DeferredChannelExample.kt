package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select
import kotlin.system.measureTimeMillis

fun main() = runBlocking {
    val elements = 10
    val deferredChannel = Channel<Deferred<Int>>(elements)

    GlobalScope.launch {
        repeat(elements) { i ->
            println("$i sent")
            deferredChannel.send(async {
                delay(if (i == 0) 1000 else 10)
                i
            })
        }
    }

    val time = measureTimeMillis {
        repeat(elements) {
            val result = select<Int> {
                deferredChannel.onReceive {
                    select {
                        it.onAwait { it }
                    }
                }
            }
            println(result)
        }
    }
    println("Took $time ms")

    val stop = GlobalScope.async {
        delay(600)
        true
    }

    val channel = Channel<Deferred<Int>>(elements)
    repeat(elements) { i ->
        channel.send(async {
            delay(i * 100L)
            i
        })
    }

    for (i in 1..10) {
        select<Unit> {
            stop.onAwait {
                channel.close()
            }
            channel.onReceive {
                println(it.await())
            }
        }
    }
}
