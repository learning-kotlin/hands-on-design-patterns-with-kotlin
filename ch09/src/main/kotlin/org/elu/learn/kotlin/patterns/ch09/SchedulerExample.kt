package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking

@ObsoleteCoroutinesApi
fun main() = runBlocking {
    println("run in global scope...")
    runAsync()

    println("run unconfined...")
    runUnconfined()

    println("run with parent context...")
    runParent()

    println("run own pool...")
    ownPool()
}

fun runAsync() = runBlocking {
    val r1 = GlobalScope.async {
        for (i in 1..1000) {
            println(Thread.currentThread().name)
            delay(1L)
        }
    }

    r1.await()
}

fun runUnconfined() = runBlocking {
    val r1 = async(Unconfined) {
        for (i in 1..1000) {
            println(Thread.currentThread().name)
            delay(1)
        }
    }
    r1.await()
}

fun runParent() = runBlocking {
    val r1 = async {
        for (i in 1..1000) {
            val parentThread = Thread.currentThread().name
            launch(coroutineContext) {
                println(Thread.currentThread().name == parentThread)
            }
            delay(1)
        }
    }
    r1.await()
}

@ObsoleteCoroutinesApi
fun ownPool() = runBlocking {
    val pool = newFixedThreadPoolContext(2, "My Own Pool")
    val r1 = async(pool) {
        for (i in 1..1000) {
            println(Thread.currentThread().name)
            delay(1)
        }
    }
    r1.await()
    pool.close()
}
