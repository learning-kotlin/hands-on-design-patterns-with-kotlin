package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.yield

fun main() {
    var counter = 0

    val jobs = List(10) {
        GlobalScope.launch {
            repeat(1000) {
                counter++
                yield()
            }
        }
    }

    runBlocking {
        jobs.forEach {
            it.join()
        }
        println(counter)
    }

    counter = 0
    val mutex = Mutex()
    val jobs2 = List(10) {
        GlobalScope.launch {
            repeat(1000) {
                try {
                    mutex.lock()
                    counter++
                } finally {
                    mutex.unlock()
                }
                yield()
            }
        }
    }

    runBlocking {
        jobs2.forEach {
            it.join()
        }
        println(counter)
    }

    counter = 0
    val jobs3 = List(10) {
        GlobalScope.launch {
            repeat(1000) {
                // next is same as in the example 2 but more concise
                mutex.withLock {
                    counter++
                }
                yield()
            }
        }
    }

    runBlocking {
        jobs3.forEach {
            it.join()
        }
        println(counter)
    }
}
