package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

fun main() = runBlocking {
    val deferred = CompletableDeferred<String>()

    GlobalScope.launch {
        delay(100L)
        if (Random.nextBoolean()) {
            deferred.complete("OK")
        } else {
            deferred.completeExceptionally(RuntimeException())
        }
    }

    println(deferred.await())
}
