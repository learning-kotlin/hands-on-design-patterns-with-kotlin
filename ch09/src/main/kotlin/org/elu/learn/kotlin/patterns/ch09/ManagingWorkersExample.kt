package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

@ObsoleteCoroutinesApi
fun main() {
    val numbers = List(10_000_000) {
        Random.nextInt()
    }

    runBlocking {
        val input = Channel<List<Int>>()
        val output = collector()
        val dividers = List(10) {
            divide(input, output)
        }

        GlobalScope.launch {
            for (c in numbers.chunked(1000)) {
                input.send(c)
            }
            input.close()
        }

        dividers.forEach {
            it.await()
        }

        output.close()

        delay(1000L)
    }
}

fun divide(input: ReceiveChannel<List<Int>>,
    output: SendChannel<Int>) = GlobalScope.async {
    var max = 0
    for (list in input) {
        for (i in list) {
            if (i > max) {
                max = i
                output.send(max)
            }
        }
    }
}

@ObsoleteCoroutinesApi
fun collector() = GlobalScope.actor<Int> {
    var max = 0
    for (i in this) {
        max = Math.max(max, i)
    }
    println(max)
}