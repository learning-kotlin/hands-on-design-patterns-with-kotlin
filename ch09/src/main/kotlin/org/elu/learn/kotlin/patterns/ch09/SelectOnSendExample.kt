package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select

@ObsoleteCoroutinesApi
fun main() {
    val batman = GlobalScope.actor<String> {
        for (c in this) {
            println("Batman is beating some sense into $c")
            delay(100L)
        }
    }

    val robin = GlobalScope.actor<String> {
        for (c in this) {
            println("Robin is beating some sense into $c")
            delay(250L)
        }
    }

    val j = GlobalScope.launch {
        for (c in listOf("Jocker", "Bane", "Penguin", "Riddler", "Killer Croc")) {
            val result = select<Pair<String, String>> {
                batman.onSend(c) {
                    Pair("Batman", c)
                }
                robin.onSend(c) {
                    Pair("Robin", c)
                }
            }
            delay(90)
            println(result)
        }
    }

    runBlocking {
        j.join()
    }
}
