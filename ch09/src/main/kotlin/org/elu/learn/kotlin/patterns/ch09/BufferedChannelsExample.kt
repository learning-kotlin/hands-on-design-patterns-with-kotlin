package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    // val channel = Channel<Int>() // this will print nothing
    val channel = Channel<Int>(5)

    val j = GlobalScope.launch {
        for (i in 1..10) {
            channel.send(i)
            println("Sent $i")
        }
    }

    j.join()

    val actor = GlobalScope.actor<Int>(capacity = 5) {

    }

    val producer = GlobalScope.produce<Int> (capacity = 10) {

    }
}
