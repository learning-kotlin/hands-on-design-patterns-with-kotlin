package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach

@ExperimentalCoroutinesApi
fun main() {
    val collector = Channel<String>()

    techBunch(collector)
    theFerge(collector)

    runBlocking {
        collector.consumeEach {
            println("Got news from $it")
        }
    }
}

private fun techBunch(collector: Channel<String>) = GlobalScope.launch {
    repeat(10) {
        delay((1..1000L).shuffled().first())
        collector.send("Tech Bunch")
    }
}

private fun theFerge(collector: Channel<String>) = GlobalScope.launch {
    repeat(10) {
        delay((1..1000L).shuffled().first())
        collector.send("The Ferge")
    }
}
