package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CountDownLatch

fun main() = runBlocking {
    println("Take 1...")
    withCountDownLatch()
    delay(100L)

    println("Take 2...")
    take2()
    delay(100L)

    println("Take 3...")
    take3()
    delay(100L)

    println("Take 4...")
    favorites()
}

fun withCountDownLatch() {
    // this solution brings a lot of problems
    val latch = CountDownLatch(3)

    var name: String? = null
    GlobalScope.launch {
        delay((1..100L).shuffled().first())
        println("Got name")
        name = "Inigo Montoya"
        latch.countDown()
    }

    var catchphrase = ""
    GlobalScope.launch {
        delay((1..100L).shuffled().first())
        println("Got catchphrase")
        catchphrase = "Hello. My name is Inigo Montoya. You killed my father. Prepare to die."
        latch.countDown()
    }

    var repeats = 0
    GlobalScope.launch {
        delay((1..100L).shuffled().first())
        println("Got repeats")
        repeats = 6
        latch.countDown()
    }

    latch.await()

    println("$name says: ${catchphrase.repeat(repeats)}")
}

data class FavoriteCharacter(val name: String,
                             val catchphrase: String,
                             val repeats: Int)

private fun getName() = GlobalScope.async {
    delay((1..100L).shuffled().first())
    println("Got name")
    "Inigo Montoya"
}

private fun getCatchphrase() = GlobalScope.async {
    delay((1..100L).shuffled().first())
    println("Got catchphrase")
    "Hello. My name is Inigo Montoya. You killed my father. Prepare to die."
}

private fun getRepeats() = GlobalScope.async {
    delay((1..100L).shuffled().first())
    println("Got repeats")
    6
}

fun take2() = runBlocking {
    // better solution without mutable variables
    val name = getName()
    val catchphrase = getCatchphrase()
    val repeats = getRepeats()

    println("${name.await()} says: ${catchphrase.await().repeat(repeats.await())}")
}

fun take3() = runBlocking {
    // even better with data class, which is a barrier
    val character = FavoriteCharacter(getName().await(), getCatchphrase().await(), getRepeats().await())

    with(character) {
        println("$name says: ${catchphrase.repeat(repeats)}")
    }

    val (name, catchphrase, repeats) = character
    println("$name says: ${catchphrase.repeat(repeats)}")
}

object Michael {
    fun getFavoriteCharacter() = GlobalScope.async {
        // Doesn't like to think much
        delay((1..100L).shuffled().first())
        FavoriteCharacter("Terminator", "Hasta la vista, baby", 1)
    }
}

object Jake {
    fun getFavoriteCharacter() = GlobalScope.async {
        // Rather thoughtful barista
        delay((1..100L).shuffled().first())
        FavoriteCharacter("Don Vito Corleone", "I'm going to make him an offer he can't refuse", 1)
    }
}

object Me {
    fun getFavoriteCharacter() = GlobalScope.async {
        // I already prepared the answer!
        FavoriteCharacter("Inigo Montoya", "Hello, my name is...", 6)
    }
}

fun favorites() = runBlocking {
    val favoriteCharacters = listOf(Me.getFavoriteCharacter().await(),
        Michael.getFavoriteCharacter().await(),
        Jake.getFavoriteCharacter().await())

    println(favoriteCharacters)
}
