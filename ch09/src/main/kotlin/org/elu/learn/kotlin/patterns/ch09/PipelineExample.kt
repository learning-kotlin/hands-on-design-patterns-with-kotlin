package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.dom.asElementList
import org.w3c.dom.Document
import org.xml.sax.InputSource
import java.io.StringReader

@ExperimentalCoroutinesApi
fun main() {
    val pagesProduced = producePages()

    val domProducer = produceDom(pagesProduced)

    val titleProducer = produceTitles(domProducer)

    /*runBlocking {
        titleProducer.consumeEach {
            println(it)
        }
    }*/

    runBlocking {
        producePages().dom().titles().consumeEach {
            println(it)
        }
    }
}

@ExperimentalCoroutinesApi
fun producePages() = GlobalScope.produce {
    fun getPages(): List<String> {
        return listOf("<html><body><H1>Cool stuff</H1></body></html>",
            "<html><body><H1>Event more stuff</H1></body></html>").shuffled()
    }
    while (this.isActive) {
        val pages = getPages()
        for (p in pages) {
            send(p)
        }
        delay(5L)
    }
}

private fun String.toSource(): InputSource {
    return InputSource(StringReader(this))
}

@ExperimentalCoroutinesApi
fun produceDom(pages: ReceiveChannel<String>) = GlobalScope.produce {
    fun parseDom(page: String): Document {
        return kotlinx.dom.parseXml(page.toSource())
    }
    for (p in pages) {
        send(parseDom(p))
    }
}

@ExperimentalCoroutinesApi
fun produceTitles(parsedPages: ReceiveChannel<Document>) = GlobalScope.produce {
    fun getTitles(dom: Document) : List<String> {
        val h1 = dom.getElementsByTagName("H1")
        return h1.asElementList().map {
            it.textContent
        }
    }

    for (page in parsedPages) {
        for (t in getTitles(page)) {
            send(t)
        }
    }
}

@ExperimentalCoroutinesApi
private fun ReceiveChannel<Document>.titles(): ReceiveChannel<String> {
    val channel = this
    fun getTitles(dom: Document): List<String> {
        val h1 = dom.getElementsByTagName("H1")
        return h1.asElementList().map {
            it.textContent
        }
    }

    return GlobalScope.produce {
        for (page in channel) {
            for (t in getTitles(page)) {
                send(t)
            }
        }
    }
}

@ExperimentalCoroutinesApi
private fun ReceiveChannel<String>.dom(): ReceiveChannel<Document> {
    val channel = this
    return GlobalScope.produce {
        for (p in channel) {
            send(kotlinx.dom.parseXml(p.toSource()))
        }
    }
}
