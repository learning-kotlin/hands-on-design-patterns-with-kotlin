package org.elu.learn.kotlin.patterns.ch09

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.selectUnbiased

@ExperimentalCoroutinesApi
fun main() {
    //failedRun()
    println("handles nulls with exception or default value")
    handleNull()
    println("skip nulls")
    skipNulls()
}

@ExperimentalCoroutinesApi
fun failedRun() = runBlocking {
    val p1 = GlobalScope.produce {
        repeat(10) {
            send("A")
        }
    }
    val p2 = GlobalScope.produce {
        repeat(5) {
            send("B")
        }
    }

    // this code will fail eventually with ClosedReceiveChannelException
    repeat(15) {
        val result = selectUnbiased<String> {
            p1.onReceive {
                it
            }
            p2.onReceive {
                it
            }
        }
        println(result)
    }
}

@ExperimentalCoroutinesApi
fun handleNull() = runBlocking {
    val p1 = GlobalScope.produce {
        repeat(10) {
            send("A")
        }
    }
    val p2 = GlobalScope.produce {
        repeat(5) {
            send("B")
        }
    }

    repeat(15) {
        val result = selectUnbiased<String> {
            p1.onReceiveOrNull {
                // Can throw own exception
                it ?: throw RuntimeException("closed")
            }
            p2.onReceiveOrNull {
                // or supply default value
                it ?: "p2 closed"
            }
        }
        println(result)
    }
}

@ExperimentalCoroutinesApi
fun skipNulls() = runBlocking {
    val p1 = GlobalScope.produce {
        repeat(10) {
            send("A")
        }
    }
    val p2 = GlobalScope.produce {
        repeat(5) {
            send("B")
        }
    }

    // or skip the null results
    var count = 0
    while (count < 15) {
        val result = selectUnbiased<String?> {
            p1.onReceiveOrNull {
                it
            }
            p2.onReceiveOrNull {
                it
            }
        }
        if (result != null) {
            println(result)
            count++
        }
    }
    println("not null values count $count")
}
