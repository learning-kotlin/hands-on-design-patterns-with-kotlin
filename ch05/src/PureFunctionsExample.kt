fun main() {
    sayHello()

    println("test hello ${testHello()}")

    println()
    val list = mutableListOf(1, 2, 3)
    println(removeFirst(list))
    println(removeFirst(list))
    println("list after $list")

    println()
    val list2 = mutableListOf(1, 2, 3)
    println(withoutFirst(list2))
    println(withoutFirst(list2))
    println("list after $list2")
}

// impure function
fun sayHello() {
    println("Hello")
}

// pure function
fun hello() = "Hello"

fun testHello(): Boolean {
    return "Hello" == hello()
}

// impure function
fun <T> removeFirst(list: MutableList<T>): T {
    return list.removeAt(0)
}

// pure function
fun <T> withoutFirst(list: List<T>): T {
    return ArrayList(list).removeAt(0)
}
