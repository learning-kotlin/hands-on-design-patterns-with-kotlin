import kotlin.concurrent.thread

fun main() {
    // tuple example
    val pair = "a" to 1

    val (key, value) = pair
    println("$key => $value")

    for (p in mapOf(1 to "Sunday", 2 to "Monday")) {
        println("${p.key} ${p.value}")
    }

    // mutable data class
    // calculateAverageAndCauseArithmeticException() // causes java.lang.ArithmeticException: / by zero

    // data class with mutable collection
    // calcScores() // causes java.lang.ArithmeticException: / by zero

    // immutable collection
    calcImmutableScores()
}

// data class with mutable properties is dangerous
data class AverageScore(var totalScore: Int = 0,
                        var gamesPlayed: Int = 0) {
    val average: Int
        get() = if (gamesPlayed <= 0)
            0
        else
            totalScore / gamesPlayed
}

fun calculateAverageAndCauseArithmeticException() {
    val counter = AverageScore()

    thread(isDaemon = true) {
        while(true) counter.gamesPlayed = 0
    }

    for (i in 1..10_000) {
        counter.totalScore += (1..100).shuffled().first()
        counter.gamesPlayed++

        println(counter.average)
    }
}

// data class with val properties but mutable collections
data class ScoreCollector(val scores: MutableList<Int> = mutableListOf())

fun calcScores() {
    val counter = ScoreCollector()

    thread(isDaemon = true, name="Maleficent") {
        while(true) counter.scores.clear()
    }

    for (i in 1..1_000) {
        counter.scores += (1..100).shuffled().first()
    }

    println(counter.scores.sumBy { it } / counter.scores.size)
}

// data class with immutable collection
data class ImmutableScoreCollector(val scores: List<Int>)

fun calcImmutableScores() {
    val counter = ImmutableScoreCollector(List(1_000) {
        (1..100).shuffled().first()
    })

    thread(isDaemon = true, name="Maleficent 2") {
        // cannot call clear, because collection is immutable
        //while(true) counter.scores.clear()
    }

    println(counter.scores.sumBy { it } / counter.scores.size)
}