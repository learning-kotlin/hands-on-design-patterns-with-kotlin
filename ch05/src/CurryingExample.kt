fun main() {
    println(subtract(50, 8))

    println(subtract2(50)(9))
    println(subtract3(50)(10))
    println(subtract4(52)(10))
}

fun subtract(x: Int, y: Int): Int {
    return x - y
}

fun subtract2(x: Int): (Int) -> Int {
    return fun(y: Int): Int {
        return x - y
    }
}

fun subtract3(x: Int) = fun(y: Int): Int {
    return x - y
}

fun subtract4(x: Int) = {y: Int -> x - y}
