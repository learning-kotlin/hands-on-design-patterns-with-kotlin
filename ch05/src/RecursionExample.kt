fun main() {
    val numbers = List(1_000_000) { it }
    println(sumRecNoTailRec(0, numbers)) // Crashed pretty soon, around 7K

    println("with tailrec")
    println(sumRec(0, 0, numbers))
}

fun sumRecNoTailRec(i: Int, numbers: List<Int>): Long {
    return try {
        if (i == numbers.size) {
            0
        } else {
            numbers[i] + sumRecNoTailRec(i + 1, numbers)
        }
    } catch (e: StackOverflowError) {
        println("I was $i")
        0
    }
}

tailrec fun sumRec(i: Int, sum: Long, numbers: List<Int>): Long {
    return if (i == numbers.size) {
        return sum
    } else {
        sumRec(i + 1, numbers[i] + sum, numbers)
    }
}
