fun main() {
    val l1 = listOf(1, 2, 3)
    val l2 = listOf(1, 2, 3)
    val l3 = listOf(1, 2, 3, 4)

    val summarizer = Summarizer()

    println(summarizer.summarize(l1)) // Computes, new input
    println(summarizer.summarize(l1)) // Object is the same, no compute
    println(summarizer.summarize(l2)) // Value is the same, no compute
    println(summarizer.summarize(l3)) // Computes
}

class Summarizer {
    private val resultsCache = mutableMapOf<List<Int>, Double>()

    fun summarize(numbers: List<Int>): Double {
        return resultsCache.computeIfAbsent(numbers, ::sum)
    }

    private fun sum(numbers: List<Int>): Double {
        return numbers.sumByDouble { it.toDouble() }
    }
}