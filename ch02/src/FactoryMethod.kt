fun main() {
    val c = catFactoryFun()
    println(c.name)
    println()

    val animalTypes = listOf("dog", "dog", "cat", "dog", "cat", "cat")

    for (t in animalTypes) {
        val a = animalFactoryFun(t)
        println(a.name)
    }
    println()

    val factory = AnimalFactory()
    for (t in animalTypes) {
        val b = factory.createAnimal(t)
        println("${b.id} - ${b.name}")
    }
    println()

    val animalTypes2 = listOf(
        "dog" to "bulldog",
        "dog" to "beagle",
        "cat" to "persian",
        "dog" to "poodle",
        "cat" to "russian blue",
        "cat" to "siamese"
    )
    for ((type, breed) in animalTypes2) {
        val d = factory.createAnimal(type, breed)
        println(d.name)
    }
}

class Cat : Animal {
    override val name = "Cat"
}

fun catFactoryFun(): Cat {
    return Cat()
}

class Dog : Animal {
    override val name = "Dog"
}

interface Animal {
    val name: String
}

fun animalFactoryFun(animalType: String): Animal {
    return when (animalType.toLowerCase()) {
        "cat" -> Cat()
        "dog" -> Dog()
        else -> throw RuntimeException("Unknown animal $animalType")
    }
}

interface AnimalTwo {
    val id: Int
    val name: String
}

open class CatTwo(override val id: Int) : AnimalTwo {
    override val name = "Cat"
}

open class DogTwo(override val id: Int) : AnimalTwo {
    override val name = "Dog"
}

class AnimalFactory {
    var counter = 0
    private val dogFactory = DogFactory()
    private val catFactory = CatFactory()

    fun createAnimal(animalType: String): AnimalTwo {
        return when (animalType.trim().toLowerCase()) {
            "cat" -> CatTwo(++counter)
            "dog" -> DogTwo(++counter)
            else -> throw RuntimeException("Unknown animal $animalType")
        }
    }

    fun createAnimal(animalType: String, animalBreed: String): AnimalTwo {
        return when(animalType.trim().toLowerCase()) {
            "cat" -> catFactory.createCat(animalBreed, ++counter)
            "dog" -> dogFactory.createDog(animalBreed, ++counter)
            else -> throw RuntimeException("Unknown animal $animalType")
        }
    }
}

class Beagle(id: Int): DogTwo(id)
class Bulldog(id: Int): DogTwo(id)
class Poodle(id: Int): DogTwo(id)
class Persian(id: Int): CatTwo(id)
class Siamese(id: Int): CatTwo(id)
class RussianBlue(id: Int): CatTwo(id)

class DogFactory {
    fun createDog(breed: String, id: Int) = when(breed.trim().toLowerCase()) {
        "beagle" -> Beagle(id)
        "bulldog" -> Bulldog(id)
        "poodle" -> Poodle(id)
        else -> throw RuntimeException("Unknown dog breed $breed")
    }
}

class CatFactory {
    fun createCat(breed: String, id: Int) = when(breed.trim().toLowerCase()) {
        "persian" -> Persian(id)
        "siamese" -> Siamese(id)
        "russian blue" -> RussianBlue(id)
        else -> throw RuntimeException("Unknown dog breed $breed")
    }
}
