package abstractFactory

fun main() {
    val hq = CatHQ()
    val barracks1 = hq.buildBarracks()
    val barracks2 = hq.buildBarracks()
    val vehicleFactory1 = hq.buildVehicleFactory()

    val units = listOf(
        barracks1.build(InfantryUnits.RIFLEMAN),
        barracks2.build(InfantryUnits.ROCKET_SOLDIER),
        barracks2.build(InfantryUnits.ROCKET_SOLDIER),
        vehicleFactory1.build(VehicleUnits.TANK),
        vehicleFactory1.build(VehicleUnits.APC),
        vehicleFactory1.build(VehicleUnits.APC)
    )

    units.forEach { println(it) }
    hq.buildings.forEach { println(it) }
}

interface Building<in UnitType, out ProducedUnit> where UnitType : Types, ProducedUnit : Unit {
    fun build(type: UnitType): ProducedUnit
}

interface HQ {
    fun buildBarracks(): Building<InfantryUnits, Infantry>
    fun buildVehicleFactory(): Building<VehicleUnits, Vehicle>
}

class CatHQ : HQ {
    val buildings = mutableListOf<Building<*, Unit>>()

    override fun buildBarracks(): Building<InfantryUnits, Infantry> {
        val b = object : Building<InfantryUnits, Infantry> {
            override fun build(type: InfantryUnits): Infantry {
                return when (type) {
                    InfantryUnits.RIFLEMAN -> Rifleman()
                    InfantryUnits.ROCKET_SOLDIER -> RocketSoldier()
                }
            }
        }
        buildings.add(b)
        return b
    }

    override fun buildVehicleFactory(): Building<VehicleUnits, Vehicle> {
        val vf = object : Building<VehicleUnits, Vehicle> {
            override fun build(type: VehicleUnits) = when (type) {
                VehicleUnits.APC -> APC()
                VehicleUnits.TANK -> Tank()
            }
        }
        buildings.add(vf)

        return vf
    }
}

interface Unit
interface Vehicle : Unit
interface Infantry : Unit

interface Types
class Rifleman : Infantry
class RocketSoldier : Infantry
enum class InfantryUnits : Types {
    RIFLEMAN,
    ROCKET_SOLDIER
}

class APC : Vehicle
class Tank : Vehicle
enum class VehicleUnits : Types {
    APC,
    TANK
}
