data class Mail(val to: String,
                var title: String = "",
                var _message: String = "",
                val cc: List<String> = listOf(),
                val bcc: List<String> = listOf(),
                val attachments: List<java.io.File> = listOf()) {
    fun message(message: String) = apply {
        _message = message
    }
}

class MailBuilder(val to: String) {
    private val mail: Mail = Mail(to)
    fun title(title: String): MailBuilder {
        mail.title = title
        return this
    }
    fun message(message: String): MailBuilder {
        mail.message(message)
        return this
    }
    fun build(): Mail {
        return mail
    }
}

fun main() {
    val email1 = MailBuilder("hello@hello.co").title("What's up?").build()
    println(email1)

    val email2 = Mail("hello@sample.com").message("How are you?")
    println(email2)
}