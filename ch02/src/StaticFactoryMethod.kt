class NumberMaster {
    companion object {
        fun valueOf(hopefullyNumber: String): Long {
            return hopefullyNumber.toLong()
        }
    }
}

fun main() {
    println(NumberMaster.valueOf("123"))
}
