fun main() {
    val numbers = (1..10).toList()

    println(numbers)
    println(numbers.slice(0..3))
    println(numbers.subList(0, 3)) // Java equivalent is not inclusive
}
