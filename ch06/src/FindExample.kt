fun main() {
    val people = listOf(
        Person("Jane", "Doe", 19),
        Person("John", "Doe", 24),
        Person("John", "Smith", 23)
    )

    println(people.find {
        it.firstName == "John"
    })

    // find already exists as some other
    println(people.findLast {
        it.firstName == "John"
    })
}

data class Person(
    val firstName: String,
    val lastName: String,
    val age: Int
)

fun <T> List<T>.find(check: (T) -> Boolean): T? {
    for (p in this) {
        if (check(p)) {
            return p
        }
    }
    return null
}
