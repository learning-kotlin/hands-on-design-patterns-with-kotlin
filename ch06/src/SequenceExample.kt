fun main() {
    // infinite sequence of numbers
    val seq = generateSequence(1) { it + 1 }

    // take first 100
    seq.take(100).forEach {
        print("$it ")
    }
    println()

    // finite number of sequences can be created by returning null
    val finiteSequence = generateSequence(1) {
        if (it < 1000) it + 1 else null
    }

    finiteSequence.forEach {
        print("$it ")
    }
    println()

    // finite sequence of numbers from range
    (1..1000).asSequence().forEach {
        print("$it ")
    }
    println()
}
