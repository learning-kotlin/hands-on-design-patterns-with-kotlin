fun main() {
    println(filter((1..10).toList()) {
        it % 2 != 0
    })

    // same with filter
    println((1..10).toList().filter {
        it % 2 != 0
    })
}

fun filter(numbers: List<Int>, check: (Int) -> Boolean): MutableList<Int> {
    val result = mutableListOf<Int>()

    for (n in numbers) {
        if (check(n)) {
            result.add(n)
        }
    }
    return result
}