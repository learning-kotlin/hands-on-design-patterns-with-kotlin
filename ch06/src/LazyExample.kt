import kotlin.streams.toList
import kotlin.system.measureTimeMillis

fun main() {
    val numbers = (1..1_000_000).toList()
    println(measureTimeMillis {
        // this is never executed
        // stream waits for terminating function call
        numbers.stream().map {
            it * it
        }
    }) // ~ 3ms

    println(measureTimeMillis {
        numbers.map {
            it * it
        }
    }) // ~ 30ms

    println(measureTimeMillis {
        numbers.stream().map {
            it * it
        }.toList() // converting to list is expensive
    }) // ~ 200ms
}
