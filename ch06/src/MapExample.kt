fun main() {
    val letters = listOf("a", "b", "c", "d")

    println(repeatAll(letters))

    println(repeatSomething(letters) {
        it.toUpperCase()
    })
    println()

    // same with .map function
    println(letters.map {
        it + it
    })
    println(letters.map {
        it.toUpperCase()
    })
    println()

    // .mapTo
    val results = mutableListOf<String>()
    results.addAll(letters.map {
        it.toUpperCase()
    })
    results.addAll(letters.map {
        it.toLowerCase()
    })
    println(results)

    // same with .mapTo
    val results2 = mutableListOf<String>()
    letters.mapTo(results2) {
        it.toUpperCase()
    }
    letters.mapTo(results2) {
        it.toLowerCase()
    }
    println(results2)
}

fun repeatAll(letters: List<String>): MutableList<String> {
    val repeatedLetters = mutableListOf<String>()

    for (l in letters) {
        repeatedLetters.add(l + l)
    }
    return repeatedLetters
}

fun <T> repeatSomething(input: List<T>, action: (T) -> T): MutableList<T> {
    val result = mutableListOf<T>()

    for (i in input) {
        result.add(action(i))
    }
    return result
}