fun main() {
    val listOfLists = listOf(listOf(1, 2),
        listOf(3, 4, 5), listOf(6, 7, 8))

    // imperative style to flatten this list
    val results = mutableListOf<Int>()
    for (l in listOfLists) {
        results.addAll(l)
    }
    println(results)
    println()

    // using functional approach
    println(listOfLists.flatten())
    println(listOfLists.flatMap {
        it.asReversed()
    })
    println(listOfLists.flatMap {
        it.map { it.toDouble() }
    })
    println()

    val setOfListsOfSets = setOf(
        listOf(setOf(1, 2), setOf(3, 4, 5), setOf(6, 7, 8)),
        listOf(setOf(9, 10), setOf(11, 12)))

    println(setOfListsOfSets)
    println(setOfListsOfSets.flatten())
    println(setOfListsOfSets.flatten().flatten())
    // println(setOfListsOfSets.flatten().flatten().flatter()) // won't compile
}
