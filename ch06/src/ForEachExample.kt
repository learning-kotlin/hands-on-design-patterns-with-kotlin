fun main() {
    val numbers = (0..5)

    // for each is terminator function, cannot be chained
    numbers.map { it * it }
        .filter { it < 20 }
        .sortedDescending()
        .forEach { println(it) }

    numbers.map { it * it }
        .forEachIndexed { index, value ->
            print("$index:$value, ")
        }
    println()

    // onEach non terminator equivalent, can be chained
    val result = numbers.map { it * it }
        .filter { it < 20 }
        .sortedDescending()
        .onEach { println(it) }
        .filter { it > 5 }
    println(result)
}
