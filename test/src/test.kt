import java.io.File

fun main(args: Array<String>) {
/*
    val file = File("dictionary.txt")

    if (args.size != 1) {
        println("Usage: script <word>")
        return
    }

    val word = args[0]

    if (file.readLines().asSequence().map { it.toLowerCase() }.contains(word.toLowerCase())) {
        println("word [$word] is in dictionary")
    }

 */
    check()
}

val file = File("dictionary.txt")

fun check() {
    val lines = file.readLines().asSequence()
    val cache = mutableMapOf<String, Boolean>()
    println("give a word (type 'quit' to quit):")
    var word = readLine()

    while(word != "quit") {
        when (cache[word]) {
            true -> println("word [$word] is in dictionary (cached)")
            false -> println("word [$word] is NOT in dictionary (cached)")
            else -> {
                if (lines.any { it.toLowerCase() == word?.toLowerCase() }) {
                    cache[word!!] = true
                    println("word [$word] is in dictionary")
                } else {
                    cache[word!!] = false
                    println("word [$word] is NOT in dictionary")
                }
            }
        }
        println("give a word (type 'quit' to quit):")
        word = readLine()
        println()
    }
    println("Quitting...")
}
