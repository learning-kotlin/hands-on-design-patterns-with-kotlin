fun main() {
    val happy = HappyMap<String, String>()
    happy["one"] = "one"
    happy["two"] = "two"
    happy["two"] = "three"
    println()

    val sadHappy = SadMap(HappierMap<String, String>())
    sadHappy["one"] = "one"
    sadHappy["two"] = "two"
    sadHappy["two"] = "three"
    sadHappy["a"] = "b"
    sadHappy.remove("a")
    println()

    println(sadHappy is SadMap<*, *>)
    println(sadHappy is HappierMap<*, *>)
    println(sadHappy is MutableMap<*, *>)
    println()

    val superSad = SadMap(SadMap<String, String>())
    superSad["a"] = "b"
    superSad.remove("a")
}

class HappyMap<K, V>: HashMap<K, V>() {
    override fun put(key: K, value: V): V? {
        return super.put(key, value).apply {
            this?.let {
                println("Yay! $key")
            }
        }
    }
}

class HappierMap<K, V>(private val map: MutableMap<K, V> = mutableMapOf()) : MutableMap<K, V> by map {
    override fun put(key: K, value: V): V? {
        return map.put(key, value).apply {
            this?.let { println("Yay! $key") }
        }
    }
}

class SadMap<K, V>(private val map: MutableMap<K, V> = mutableMapOf()) : MutableMap<K, V> by map {
    override fun remove(key: K): V? {
        println("Okay...")
        return map.remove(key)
    }
}
