import java.util.stream.Stream
import kotlin.streams.toList

fun main() {
    cellPhone(charger(powerOutlet().toEUPlug()).toUsbTypeC())

    val l = listOf("a", "b", "c")
    streamProcessing(l.stream())

    val s = Stream.generate { 42 }
    println("Collecting elements")
    // the next line will never ends and will produce out of memory error
    // collectionProcessing(s.toList())
}

fun <T> collectionProcessing(c: Collection<T>) {
    for (e in c) {
        println(e)
    }
}

fun <T> streamProcessing(stream: Stream<T>) {
    // do something with stream
}

interface UsbTypeC
interface  UsbMini

fun UsbMini.toUsbTypeC() : UsbTypeC {
    return object : UsbTypeC {
        // do something to convert
    }
}

interface EUPlug
interface CHPlug

fun CHPlug.toEUPlug(): EUPlug {
    return object : EUPlug {
        // do something to convert
    }
}

fun powerOutlet(): CHPlug {
    return object: CHPlug {}
}

fun cellPhone(chargeCable: UsbTypeC) {}

fun charger(plug: EUPlug): UsbMini {
    return object: UsbMini {}
}