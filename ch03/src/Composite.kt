fun main() {
    val miller = Rifleman()
    val caparzo = Rifleman()
    val jackson = Sniper()

    val squad1 = Squad()

    squad1.infantryUnits.add(miller)
    squad1.infantryUnits.add(caparzo)
    squad1.infantryUnits.add(jackson)

    println(squad1.infantryUnits.size)
    println()

    val squad2 = Squad(miller, caparzo, jackson)
    println(squad2.bulletsLeft())
}

interface InfantryUnit : CanCountBullets

class Rifleman(initialMagazines: Int = 3) : InfantryUnit {
    private val magazines = List(initialMagazines) {
        Magazine(5)
    }

    override fun bulletsLeft() = magazines.sumBy { it.bulletsLeft() }
}

class Sniper(initialBullets: Int = 50) : InfantryUnit {
    private val bullets = List(initialBullets) { Bullet() }
    override fun bulletsLeft() = bullets.size
}

class Squad(val infantryUnits: MutableList<InfantryUnit> = mutableListOf()): CanCountBullets {
    constructor(vararg units: InfantryUnit) : this(mutableListOf()) {
        for (u in units) {
            this.infantryUnits.add(u)
        }
    }

    override fun bulletsLeft() = infantryUnits.sumBy { it.bulletsLeft() }
}

class Bullet

class Magazine(capacity: Int): CanCountBullets {
    private val bullets = List(capacity) { Bullet() }

    override fun bulletsLeft() = bullets.size
}

interface CanCountBullets {
    fun bulletsLeft(): Int
}
