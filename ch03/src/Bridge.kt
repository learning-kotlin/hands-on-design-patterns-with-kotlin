fun main() {
    val rifleman = Soldier(Rifle(), RegularLegs(), "Hurray")
    val grenadier = Soldier(Grenade(), RegularLegs(), "Hura")
    val upgradedGrenadier = Soldier(GrenadePack(), RegularLegs(), "Kaboom")
    val upgradedRifleman = Soldier(MachineGun(), RegularLegs(), "Foobar")
    val lightRifleman = Soldier(Rifle(), AthleticLegs(), "Wooza")
    val lightGrenadier = Soldier(Grenade(), AthleticLegs(), "Banzai")

    val army = listOf(rifleman, grenadier, upgradedGrenadier, upgradedRifleman, lightRifleman, lightGrenadier)
    var x = 0L
    var y = 20L
    for (unit in army) {
        unit.shout()
        println("distance: ${unit.move(++x, --y)}")
        println("damage: ${unit.attack(++x, --y)}")
        println()
    }
}

interface Infantry {
    fun move(x: Long, y: Long): Meters
    fun attack(x: Long, y: Long): PointsOfDamage

    fun shout() // Here comes the change, which requires changes to all classes
}

class Soldier(private val weapon: Weapon,
              private val legs: Legs,
              private val battleCry: String) : Infantry {
    override fun move(x: Long, y: Long): Meters {
        println("Compute direction for ($x, $y)")
        println("Move at its own pace")
        return legs.move()
    }

    override fun attack(x: Long, y: Long): PointsOfDamage {
        println("Find target at ($x, $y)")
        println("Shoot")
        return weapon.causeDamage()
    }

    override fun shout() {
        println(battleCry)
    }
}
typealias PointsOfDamage = Long
typealias Meters = Int

interface Weapon {
    fun causeDamage(): PointsOfDamage
}

interface Legs {
    fun move(): Meters
}

const val GRENADE_DAMAGE : PointsOfDamage = 5L
const val RIFLE_DAMAGE = 3L
const val REGULAR_SPEED : Meters = 1

class Grenade : Weapon {
    override fun causeDamage() = GRENADE_DAMAGE
}

class GrenadePack : Weapon {
    override fun causeDamage() = GRENADE_DAMAGE * 3
}

class Rifle : Weapon {
    override fun causeDamage() = RIFLE_DAMAGE
}

class MachineGun : Weapon {
    override fun causeDamage() = RIFLE_DAMAGE * 2
}

class RegularLegs : Legs {
    override fun move() = REGULAR_SPEED
}

class AthleticLegs : Legs {
    override fun move() = REGULAR_SPEED * 2
}

// Abuse of class inheritance
/*
open class Rifleman : Infantry {
    override fun move(x: Long, y: Long) {
        println("Move at its own pace toward ($x, $y)")
    }

    override fun attack(x: Long, y: Long) {
        println("Shoot to ($x, $y)")
    }
}

open class Grenadier : Infantry {
    override fun move(x: Long, y: Long) {
        println("Move slowly, grenades are heavy toward ($x, $y)")
    }

    override fun attack(x: Long, y: Long) {
        println("Throw grenades to ($x, $y)")
    }
}

class UpgradedRifleman : Rifleman() {
    override fun attack(x: Long, y: Long) {
        println("Shoot twice as much to ($x, $y)")
    }
}

class UpgradeGrenadier : Grenadier() {
    override fun attack(x: Long, y: Long) {
        println("Throw pack of grenades to ($x, $y)")
    }
}

class LightRifleman : Rifleman() {
    override fun move(x: Long, y: Long) {
        println("Running with rifle to ($x, $y)")
    }
}

class LightGrenadier : Grenadier() {
    override fun move(x: Long, y: Long) {
        println("I've been to gym, pack of grenades is no problem, running to ($x, $y)")
    }
}
*/
