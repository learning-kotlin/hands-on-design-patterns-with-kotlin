fun main() {
    val h = OurHero()
    h.shoot()
    h.currentWeapon = Weapons.banana
    h.shoot()
}

enum class Direction {
    LEFT, RIGHT
}

abstract class Projectile(private val x: Int,
                          private val y: Int,
                          private val direction: Direction)

class OurHero {
    private var direction = Direction.LEFT
    private var x: Int = 42
    private var y: Int = 173

    var currentWeapon = Weapons.peashooter

    fun shoot() = fun() {
        currentWeapon(x, y, direction)
    }
}

object Weapons {
    val peashooter = fun(x: Int, y: Int, direction: Direction): Projectile {
        return object : Projectile(x, y, direction) {}
    }
    val banana = fun(x: Int, y: Int, direction: Direction): Projectile {
        return object : Projectile(x, y, direction) {}
    }
    val pomegranate = fun(x: Int, y: Int, direction: Direction): Projectile {
        return object : Projectile(x, y, direction) {}
    }
}
