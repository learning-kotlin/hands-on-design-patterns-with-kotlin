fun main() {
    val s = Soldier()
    s.appendMove(20, 0)
        .appendMove(20, 20)
        .appendMove(5, 20)
        .execute()
}

typealias Command = () -> Unit

val moveGenerator = fun(s: Soldier,
                        x: Int,
                        y: Int): Command {
    return fun() {
        s.move(x, y)
    }
}

class Soldier {
    private val commands = mutableListOf<Command>()

    fun execute() {
        while (commands.isNotEmpty()) {
            val command = commands.removeAt(0)
            command.invoke()
        }
    }

    fun move(x: Int, y: Int) {
        println("Moving to ($x, $y)")
    }

    fun attack(x: Int, y: Int) {
        println("Attacking ($x, $y)")
    }

    fun appendMove(x: Int, y: Int) = apply {
        commands.add(moveGenerator(this, x, y))
    }
}
