fun main() {
    //
}

interface WhatCanHappen {
    fun seeHero()
    fun getHit(pointsOfDamage: Int)
    fun timePassed()
}

class Snail : WhatCanHappen {
    private var healthPoints = 10
    private var mood: Mood = Still()

    override fun seeHero() {
        mood = when(mood) {
            is Still -> Aggressive()
//            is Aggressive -> mood
//            is Retreating -> mood
//            is Dead -> mood
            else -> mood
        }
    }

    override fun getHit(pointsOfDamage: Int) {
        healthPoints -= pointsOfDamage

        mood = when {
            (healthPoints <= 0) -> Dead()
            mood is Aggressive -> Retreating()
            else -> mood
        }
    }

    override fun timePassed() {
        mood = when(mood) {
            is Retreating -> Aggressive()
            else -> mood
        }
    }
}

sealed class Mood {
    // Some abstract methods here, like draw(), for example
}

class Still : Mood()

class Aggressive : Mood()

class Retreating : Mood()

class Dead : Mood()
