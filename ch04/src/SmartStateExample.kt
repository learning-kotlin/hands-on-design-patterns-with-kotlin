package smart.state

fun main() {
    val s = Snail()

    val still = Still(s)

    val a = still.seeHero()

    //println(a is Still)
    println(a is Aggressive)

    val b = still.timePassed()

    println(b is Still)
}

class Snail {
    internal var mood: Mood = Still(this)
    private val healthPoints = 10
}

interface WhatCanHappen {
    fun seeHero(): Mood
    fun getHit(pointsOfDamage: Int): Mood
    fun timePassed(): Mood
}

sealed class Mood : WhatCanHappen {
    override fun seeHero() = this
    override fun getHit(pointsOfDamage: Int) = this
    override fun timePassed() = this
}

class Still(private val snail: Snail) : Mood() {
    override fun seeHero() = snail.mood.run {
        Aggressive(snail)
    }
}

class Aggressive(private val snail: Snail) : Mood() {}

class Retreating : Mood()

class Dead : Mood()
