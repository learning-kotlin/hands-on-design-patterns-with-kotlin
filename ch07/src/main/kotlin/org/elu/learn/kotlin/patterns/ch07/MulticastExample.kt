package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlin.concurrent.thread

fun main() {
    val iterator = (1..5).iterator()
    val subject = Observable.create<Int> {
        while (iterator.hasNext()) {
            val number = iterator.nextInt()
            println("P: $number")
            it.onNext(number)
            Thread.sleep(10)
        }
    }.observeOn(Schedulers.newThread()).publish()

    thread {
        subject.connect()
    }

    Thread.sleep(10)
    println("S1 Subscribes")
    subject.subscribeOn(Schedulers.newThread()).subscribe {
        println("S1: $it")
        Thread.sleep(100)
    }

    Thread.sleep(20)

    println("S2 Subscribes")
    subject.subscribeOn(Schedulers.newThread()).subscribe {
        println("S2: $it")
        Thread.sleep(100)
    }
    Thread.sleep(2000)

    // This is a connectable Observable
    val connectableSource = Observable.fromIterable((1..3)).publish()

    // Should call connect() on it
    connectableSource.connect()

    // This is regular Observable which wraps ConnectableObservable
    val regularSource = connectableSource.refCount()

    // regularSource.connect() // Doesn't compile

    val anotherRegularSource = Observable.fromIterable((1..3)).publish().refCount()

    val stillRegular = Observable.fromIterable((1..3)).share()
}
