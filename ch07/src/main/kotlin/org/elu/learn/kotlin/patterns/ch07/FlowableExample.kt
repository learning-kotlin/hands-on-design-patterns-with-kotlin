package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.BackpressureStrategy
import io.reactivex.Emitter
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger

fun main() {
    //observable()
    // flowable()
    // flowableAnonymous() // always throws MissingBackpressureException
    // generate()
    generateFunctional()
}

fun observable() {
    val latch = CountDownLatch(1)
    val source = Observable.create<String> {
        var startProducing = System.currentTimeMillis()
        for (i in 1..10_000_000) {
            it.onNext(UUID.randomUUID().toString())

            if (i % 100_000 == 0) {
                println("Produced.1 $i events in ${System.currentTimeMillis() - startProducing}ms")
                startProducing = System.currentTimeMillis()
            }
        }
        latch.countDown()
    }

    val counter = AtomicInteger(0)
    source.subscribeOn(Schedulers.newThread())
        .subscribe({
            it.repeat(500)
            if (counter.incrementAndGet() % 100_000 == 0) {
                println("Consumed.1 ${counter.get()} events")
            }
        }, {
            println(it)
        })

    latch.await()
}

fun flowable() {
    val latch = CountDownLatch(1)
    val source = Flowable.create<String>({
        var startProducing = System.currentTimeMillis()
        for (i in 1..10_000_000) {
            it.onNext(UUID.randomUUID().toString())

            if (i % 100_000 == 0) {
                println("Produced.2 $i events in ${System.currentTimeMillis() - startProducing}ms")
                startProducing = System.currentTimeMillis()
            }
        }
        it.onComplete()
        latch.countDown()
    }, BackpressureStrategy.DROP)

    val counter = AtomicInteger(0)
    source.subscribeOn(Schedulers.newThread())
        .subscribe({
            it.repeat(500)
            if (counter.incrementAndGet() % 100_000 == 0) {
                println("Consumed.2 ${counter.get()} events")
            }
        }, {
            println(it)
        })

    latch.await()
}

fun flowableAnonymous() {
    val latch = CountDownLatch(1)
    val source = Flowable.create<String>({
        var startProducing = System.currentTimeMillis()
        for (i in 1..10_000_000) {
            it.onNext(UUID.randomUUID().toString())

            if (i % 100_000 == 0) {
                println("Produced.3 $i events in ${System.currentTimeMillis() - startProducing}ms")
                startProducing = System.currentTimeMillis()
            }
        }
        it.onComplete()
        latch.countDown()
    }, BackpressureStrategy.MISSING).onBackpressureBuffer(10_000)

    val counter = AtomicInteger(0)
    source.observeOn(Schedulers.newThread())
        .subscribe(object : Subscriber<String> {
            lateinit var subscription: Subscription

            override fun onSubscribe(s: Subscription?) {
                s?.let {
                    this.subscription = it
                    this.subscription.request(100)
                } ?: throw RuntimeException()
            }

            override fun onNext(t: String?) {
                t?.repeat(500) // Do something

                println(counter.get()) // Print index of this item
                this.subscription.request(1) // Request next

                if (counter.incrementAndGet() % 100_000 == 0) {
                    println("Consumed.3 ${counter.get()} events")
                }
            }

            override fun onError(t: Throwable?) {
                t?.printStackTrace()
            }

            override fun onComplete() {
                println("Completing.3...")
            }
        })

    latch.await()
}

fun generate() {
    val latch = CountDownLatch(1)
    val count = AtomicInteger(0)
    var startTime = System.currentTimeMillis()
    val source = Flowable.generate<String> {
        it.onNext(UUID.randomUUID().toString())

        if (count.incrementAndGet() == 10_000_000) {
            it.onComplete()
            latch.countDown()
        }

        if (count.get() % 100_000 == 0) {
            println("Produced.4 ${count.get()} events in ${System.currentTimeMillis() - startTime}ms")
            startTime = System.currentTimeMillis()
        }
    }

    source.observeOn(Schedulers.newThread()).subscribe(object: Subscriber<String> {
        lateinit var subscription : Subscription

        override fun onNext(t: String) {
            t.repeat(500)

            this.subscription.request(1)
        }

        override fun onError(t: Throwable?) {
            println(t)
        }

        override fun onSubscribe(s: Subscription) {
            this.subscription = s
            this.subscription.request(1)
        }

        override fun onComplete() {
            println("Completing.4...")
        }
    })

    latch.await()
}

data class State(val count: Int, val startTime: Long)

fun generateFunctional() {
    val latch = CountDownLatch(1)
    val source = Flowable.generate<String, State>(
        Callable<State> { State(0, System.currentTimeMillis()) },
        BiFunction<State, Emitter<String>, State> { state, emitter ->
            emitter.onNext(UUID.randomUUID().toString())

            val count = state.count + 1
            var startTime = state.startTime
            if (count == 10_000_000) {
                emitter.onComplete()
                latch.countDown()
            }

            if (count % 100_000 == 0) {
                println("Produced.5 $count events in ${System.currentTimeMillis() - startTime}ms")
                startTime = System.currentTimeMillis()
            }

            State(count, startTime)
        }
    )

    source.observeOn(Schedulers.newThread()).subscribe(object: Subscriber<String> {
        lateinit var subscription : Subscription

        override fun onNext(t: String) {
            t.repeat(500)

            this.subscription.request(1)
        }

        override fun onError(t: Throwable?) {
            println(t)
        }

        override fun onSubscribe(s: Subscription) {
            this.subscription = s
            this.subscription.request(1)
        }

        override fun onComplete() {
            println("Completing.5...")
        }
    })

    latch.await()
}
