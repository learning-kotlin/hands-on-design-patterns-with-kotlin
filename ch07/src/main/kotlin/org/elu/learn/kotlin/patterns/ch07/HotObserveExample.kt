package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun main() {
    val iterator = (1..10).iterator()

    val publisher = Observable.create<Int> {
        while(iterator.hasNext()) {
            val nextValue = iterator.nextInt()
            it.onNext(nextValue)
        }
    }

    publisher.subscribeOn(Schedulers.newThread()).subscribe {
        println("S1: $it")
        Thread.sleep(10)
    }

    publisher.subscribeOn(Schedulers.newThread()).subscribe {
        println("S2: $it")
        Thread.sleep(10)
    }

    Thread.sleep(50)
}