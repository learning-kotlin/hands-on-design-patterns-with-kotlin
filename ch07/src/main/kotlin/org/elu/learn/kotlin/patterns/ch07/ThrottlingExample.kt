package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

fun main() {
    throttleFirst()
    throttleLast()
}

fun throttleFirst() {
    println("Throttle first")
    val o = PublishSubject.intervalRange(8L, 15L, 0L, 100L, TimeUnit.MILLISECONDS).publish()

    o.throttleFirst(280L, TimeUnit.MILLISECONDS).subscribe {
        println("throttle.1 $it")
    }

    o.buffer(280L, TimeUnit.MILLISECONDS).subscribe {
        println("buffer.1 $it")
    }

    o.connect()

    Thread.sleep(100 * 15)
}
fun throttleLast() {
    println("Throttle last")
    val o = PublishSubject.intervalRange(8L, 15L, 0L, 100L, TimeUnit.MILLISECONDS).publish()

    o.throttleLast(280L, TimeUnit.MILLISECONDS).subscribe {
        println("throttle.1 $it")
    }

    o.buffer(280L, TimeUnit.MILLISECONDS).subscribe {
        println("buffer.1 $it")
    }

    o.connect()

    Thread.sleep(100 * 15)
}
