package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.Observable
import io.reactivex.subjects.AsyncSubject
import java.util.concurrent.TimeUnit

fun main() {
    asyncSubject()
}

fun asyncSubject() {
    val list = (8..23).toList() // Some non trivial numbers
    val iterator = list.iterator()
    val o = Observable.intervalRange(0, list.size.toLong(), 0, 10, TimeUnit.MILLISECONDS).map {
        iterator.next()
    }.publish()

    val subject = AsyncSubject.create<Int>()

    o.subscribe(subject)

    o.connect() // Start publishing

    Thread.sleep(20)

    println("S1 subscribes")
    subject.subscribe {
        println("S1 $it")
    }
    println("S1 subscribed")

    Thread.sleep(10)

    println("S2 subscribes")
    subject.subscribe {
        println("S2 $it")
    }
    println("S2 subscribed")

    Thread.sleep(1000)

}
