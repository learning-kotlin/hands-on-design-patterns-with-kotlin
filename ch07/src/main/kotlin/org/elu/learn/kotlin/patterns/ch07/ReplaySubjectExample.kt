package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import java.util.concurrent.TimeUnit

fun main() {
    publishSubject()
    println("====")
    replaySubject()
    println("====")
    replaySubjectWithSize()
}

fun publishSubject() {
    val list = (8..23).toList() // Some non trivial numbers
    val iterator = list.iterator()
    val o = Observable.intervalRange(0, list.size.toLong(), 0, 10, TimeUnit.MILLISECONDS).map {
        iterator.next()
    }.publish()

    val subject = PublishSubject.create<Int>()

    o.subscribe(subject)

    o.connect() // Start publishing

    Thread.sleep(20)

    println("S1 subscribes")
    subject.subscribe {
        println("S1 $it")
    }
    println("S1 subscribed")

    Thread.sleep(10)

    println("S2 subscribes")
    subject.subscribe {
        println("S2 $it")
    }
    println("S2 subscribed")

    Thread.sleep(1000)
}

fun replaySubject() {
    val list = (8..23).toList() // Some non trivial numbers
    val iterator = list.iterator()
    val o = Observable.intervalRange(0, list.size.toLong(), 0, 10, TimeUnit.MILLISECONDS).map {
        iterator.next()
    }.publish()

    val subject = ReplaySubject.create<Int>()

    o.subscribe(subject)

    o.connect() // Start publishing

    Thread.sleep(20)

    println("S1.r subscribes")
    subject.subscribe {
        println("S1.r $it")
    }
    println("S1.r subscribed")

    Thread.sleep(10)

    println("S2.r subscribes")
    subject.subscribe {
        println("S2.r $it")
    }
    println("S2.r subscribed")

    Thread.sleep(1000)
}

fun replaySubjectWithSize() {
    val list = (8..23).toList() // Some non trivial numbers
    val iterator = list.iterator()
    val o = Observable.intervalRange(0, list.size.toLong(), 0, 10, TimeUnit.MILLISECONDS).map {
        iterator.next()
    }.publish()

    val subject = ReplaySubject.createWithSize<Int>(2)

    o.subscribe(subject)

    o.connect() // Start publishing

    Thread.sleep(20)

    println("S1.rs subscribes")
    subject.subscribe {
        println("S1.rs $it")
    }
    println("S1.rs subscribed")

    Thread.sleep(10)

    println("S2.rs subscribes")
    subject.subscribe {
        println("S2.rs $it")
    }
    println("S2.rs subscribed")

    Thread.sleep(1000)
}
