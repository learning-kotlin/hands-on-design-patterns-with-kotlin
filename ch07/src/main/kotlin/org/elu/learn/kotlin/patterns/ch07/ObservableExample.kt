package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.Observable
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit

fun main() {
    val publisher = Observable.fromArray(5, 6, 7)

    publisher.subscribe {
        println(it)
    }
    println()

    publisher.filter {
        it > 5
    }.map {
        it + it
    }.subscribe {
        println(it)
    }
    println()

    val publisher2 = Observable.interval(1, TimeUnit.SECONDS)
    publisher2.subscribe {
        println("P1 $it")
    }
    publisher2.subscribe {
        println("P2 $it")
    }
    println("Sleeping")
    Thread.sleep(TimeUnit.SECONDS.toMillis(5))

    val subscription = publisher2.subscribe {
        println("P2.2 $it")
    }
    println("Sleeping")
    Thread.sleep(TimeUnit.SECONDS.toMillis(2))
    subscription.dispose()

    val o = Observable.create<Int> {
        for (i in 1..10_000) {
            it.onNext(i)
        }
        it.onComplete()
    }
    o.subscribe {
        println("All went good: $it")
    }

    val o1 = Observable.create<Int> {
        it.onError(RuntimeException())
    }
    // Next will fail with exception: The exception was not handled due to missing onError handler
/*
    o1.subscribe {
        println("All went good 2: $it")
    }
*/
    // correct way to subscribe to such observable
    o1.subscribe({
        println("All went good 3: $it")
    }, {
        println("There was an error $it")
    })

    // same but with on complete
    o.subscribe({
        println("All went good 4: $it")
    }, {
        println("There was an error $it")
    }, {
        println("Publisher closed the stream")
    })
}
