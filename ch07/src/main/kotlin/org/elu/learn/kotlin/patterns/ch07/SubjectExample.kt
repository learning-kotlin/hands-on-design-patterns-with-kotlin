package org.elu.learn.kotlin.patterns.ch07

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

fun main() {
    // cold data source
    val dataSourceCold = Observable.fromIterable((1..3))

    val multicastCold = PublishSubject.create<Int>()

    multicastCold.subscribe {
        println("S1.c $it")
    }

    multicastCold.subscribe {
        println("S2.c $it")
    }

    dataSourceCold.subscribe(multicastCold)

    Thread.sleep(1000)

    // hot data source
    val dataSourceHot = Observable.fromIterable((1..3)).publish()

    val multicastHot = PublishSubject.create<Int>()

    dataSourceHot.subscribe(multicastHot)

    multicastHot.subscribe {
        println("S1.h $it")
    }
    println("S1.h subscribed")

    multicastHot.subscribe {
        println("S2.h $it")
    }
    println("S2.h subscribed")

    dataSourceHot.connect()

    Thread.sleep(1000)
}
